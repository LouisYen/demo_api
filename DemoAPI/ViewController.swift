//
//  ViewController.swift
//  DemoAPI
//
//  Created by 顏淩育 on 2019/9/25.
//  Copyright © 2019 顏淩育. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView! {
        didSet {
            self.textView.backgroundColor = .yellow
            self.textView.textColor = .black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlStr = "http://fin-saver.com:5000/api/demo"
        
        
        if let url = URL(string: urlStr) {

            do {
                let data: Data = try Data(contentsOf: url)

                let json: Any = try JSONSerialization.jsonObject(with: data)
                
                if let rootDictionary: [String: Any] = json as? [String: Any] {

                    if let timeValue: Any = rootDictionary["datetime"],  let metaValue: Any = rootDictionary["metaSetting"], let ogValue: Any = rootDictionary["ogSetting"] {

                        let time: String = timeValue as? String ?? "null"
                        let meta: String = metaValue as? String ?? "null"
                        let og: String = ogValue as? String ?? "null"
                        textView.text = ("datetime: \(time)\n") + ("metaSetting: \(meta)\n") + ("ogSetting: \(og)")

                        print(timeValue, metaValue, ogValue)
                    }

                } else {
                    print("error")
                }
            } catch {
                print(error)
            }

        } else {
            print("error")
        }
        
        
    }


}

struct Model: Codable {
    var datatime: String
    var metaSetting: MetaSetting
    var ogSetting: OgSetting
}

struct MetaSetting: Codable {
    var description: String
    var title: String
}

struct OgSetting: Codable {
    var description: String
    var title: String
    var url: String
}



